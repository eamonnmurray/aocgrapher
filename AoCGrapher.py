#!/usr/bin/env python3

'''Get an adventofcode private leaderboard and plot time to each star.'''

import datetime, json, requests, argparse, configparser
import errno, os, os.path

import numpy as np
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from cycler import cycler

def guess_year():
    '''Return the most recent year advent of code started'''
    today = datetime.datetime.utcnow()
    if today.month == 12 and today.time().hour >= 5:
        return today.year
    else:
        return today.year - 1

def get_leaderboard_web(year, board_id, sessioncookie):
    '''Download a private leaderboard json file from the provided url'''

    url = ("https://adventofcode.com/{}/leaderboard/private/view/{}.json"
            .format(year, board_id))
    r = requests.get(url, cookies={"session": sessioncookie})
    if r.status_code != requests.codes.ok:
        print("Error retrieving leaderboard")
        exit(1)
    return r.json()

def get_leaderboard(local, year, board_id, sessioncookie):
    '''Get and format the json leaderboard from a file if recent or the web'''
    # Check a downloaded file exists. The name is assumed to be
    # board_id_year.json (e.g. 11111_2018.json).
    board_file = "%s_%d.json" %(board_id, year)
    get_web_board = False
    current_time = datetime.datetime.utcnow().timestamp()
    if os.path.isfile(board_file):
        with open(board_file) as f:
            board = json.load(f)
            if "update_time" in board:
                if current_time - board["update_time"] > 900 and not local:
                    get_web_board = True
            else:
                # Set update_time to the file modification time if it's not
                # present. This would be the case for a manually downloaded
                # json file.
                board["update_time"] = os.path.getmtime(board_file)
                with open(board_file, 'w') as f:
                    json.dump(board, f)
    else:
        if local:
            # We can't open the file that was requested to be used.
            # Raise the appropriate error.
            raise OSError(errno.ENOENT, os.strerror(errno.ENOENT), board_file)
        else:
            get_web_board = True
    if get_web_board:
        board = get_leaderboard_web(year, board_id, sessioncookie)
        board["update_time"] = current_time
        # dump this to file to be used next time
        with open(board_file, 'w') as f:
            json.dump(board, f)
    return board

def gen_t0(year):
    '''Generate the start time for each day of the competition in "year"'''
    # set t0 = 5am UTC on Dec 1st as a unix timestamp
    t0_per_day = []
    for day in range(1, 26):
        t0_per_day.append(datetime.datetime(year, 12, day, 5).timestamp())
    return t0_per_day

def parse_member_details(member_details, year):
    '''Parse details of each member into a new list

    A row is created for each person, with a dict of their name, score, stars
    and a list of the times they got the stars for each day.
    '''
    table = []
    t0_per_day = gen_t0(year)
    for m in member_details.values():
        days = list(int(d) for d in m["completion_day_level"].keys())
        timings = []
        for day, star_times in m["completion_day_level"].items():
            day_index = int(day) - 1
            time_ele = [int(day)]
            day_stars = sorted(list(star_times.keys()))
            for day_star in day_stars:
                ts = int(star_times[day_star]['get_star_ts'])
                dt = ts - t0_per_day[day_index]
                time_ele.extend([dt / 60])
            timings.append(time_ele)
        timings.sort()
        table.append({"name": m["name"], "score": m["local_score"],
            "stars": m["stars"], "timings": timings})
    table.sort(key=lambda s: (-s["score"], -s["stars"]))
    return table

def gen_dscore(table):
    '''Generate a dscore for each user, based on time between stars 1 and 2'''

    # First generate a list of users and dt for each day. Then scores will be
    # assigned in the same way as in the private leaderboard. For an x person
    # leaderboard, first gets x points, second gets x-1 etc, but with only
    # one score per day based on shortest time between star 1 and 2.

    nusers = len(table)
    scores = [nusers - x for x in range(nusers)]
    # Get the number of days we need to rank first.
    max_days = 0
    for user in table:
        if len(user['timings']) > 0:
            last_day = user['timings'][-1][0]
            if last_day > max_days:
                max_days = last_day
        # Also initialize dscore for everyone
        user['dscore'] = 0
    # Negative will indicate no valid score for that day.
    dtimes = [[-1]*nusers for i in range(max_days)]
    for i, user in enumerate(table):
        for day_info in user['timings']:
            if len(day_info) > 2:
                dtimes[day_info[0] - 1][i] = day_info[2] - day_info[1]
    for day in range(max_days):
        score_order = [j[0] for j in sorted(enumerate(dtimes[day]),
            key=lambda x:x[1])]
        # Now assign scores to users, skipping people with negative scores.
        score_index = 0
        for user_index in score_order:
            if dtimes[day][user_index] < 0:
                continue
            table[user_index]['dscore'] += scores[score_index]
            score_index += 1

def gen_plot(table, update_time, outputfile="score_time_per_day.png"):
    '''Generate a plot showing time to star 1 and time from star 1 to 2

    The provided table is parsed to work the data into a plottable format.
    The sorted scoreboard is given in the legend for any member who has
    at least one star. The plot is saved to the provided filename which
    is "score_time_per_day.png" by default.
    '''
    # Generate a plot
    plt.style.use('ggplot')
    fig, axs = plt.subplots(2, 1, sharex=True)
    # Shrink and lighten the default title a little.
    axs[0].set_title("Advent of Code - Stars and Times as of " +
            datetime.datetime.utcfromtimestamp(update_time).
            strftime('%Y-%m-%d %H:%M') + " UTC", fontsize='medium', alpha=0.8)
    axs[1].set_xlabel("Day")
    axs[0].set_ylabel("Time to star 1 (mins)")
    axs[1].set_ylabel("Time between star\n1 and 2 (mins)")
    max_time = [0, 0] # Used to set the yranges
    last_day = 0 # used to set the xtics

    # The style may have fewer colors than users, so generate a cycle that
    # will cycle markers after we used all the colors.
    default_colors = []
    for i, color in enumerate(plt.rcParams['axes.prop_cycle']):
        default_colors.append(color['color'])
    color_cycle = cycler(c=default_colors)
    marker_cycle = cycler('marker', ['o', 's', '^', 'v', '+', 'x', 'D'])
    sty_cycle = marker_cycle * color_cycle
    style_index = 0
    # Plot the data
    for user in table:
        if len(user["timings"]) > 0:
            days = np.zeros((len(user["timings"])))
            t1 = days.copy()
            t2 = t1.copy()
            for i, d in enumerate(user["timings"]):
                days[i] = d[0]
                t1[i] = d[1]
                if len(d) > 2:
                    t2[i] = d[2]
            # Set the color and marker for this user.
            axs[0].set_prop_cycle(sty_cycle[style_index:style_index+1])
            axs[1].set_prop_cycle(sty_cycle[style_index:style_index+1])
            # Semilog plots since the range is quite large.
            axs[0].semilogy(days, t1)
            axs[1].semilogy(np.ma.array(days, mask=t2<=0),
                    np.ma.array(t2 - t1, mask=t2<=0))
            style_index += 1
            # Test if max_time and last_day need to be updated.
            if t1.max() > max_time[0]:
                max_time[0] = t1.max()
            if (t2 - t1).max() > max_time[1]:
                max_time[1] = (t2 - t1).max()
            if days[-1] > last_day:
                last_day = int(days[-1])

    axs[1].set_xticks(np.linspace(1, last_day, last_day, dtype=int))
    # Set the score board in the legend with alginment using hacky columns
    scores = [["Score", "Stars", "DTS", "Name"]]
    for user in table:
        if user["score"] > 0:
            scores.append([user["score"], user["stars"], user["dscore"],
                user["name"]])
    empty = matplotlib.lines.Line2D([0],[0],visible=False)
    # Reorder the scores list so the columns get filled in the right order
    leg_labels = np.asarray(scores).T.reshape(-1).tolist()
    # Fill in empty spaces so the columns get populated correctly
    leg_handles = [empty] + axs[0].lines + [empty] * len(scores) * 3
    # We put the legend off to the right of the plot - roughly in the centre
    leg = axs[0].legend(handles=leg_handles, labels=leg_labels, ncol=4,
        columnspacing=-2, loc=6, bbox_to_anchor=(1.02, 0), borderaxespad=0.,
        handleheight=1.6)
    # Right align the columns with numbers
    plt.setp(leg.get_texts()[:3 * len(scores)], ha='right', position=(30, 0))

    # Increase the max_time by 40% so that markers will fit in the plot.
    max_time = [t * 1.4 for t in max_time]
    axs[0].set_ylim([1, max_time[0]])
    axs[1].set_ylim([0.1, max_time[1]])
    # Extra option here ensures the legend doesn't get cut off.
    fig.savefig(outputfile, bbox_inches='tight')

def parse_input_args():
    '''Parse the year, sessioncookie and leaderboard id as arguments'''

    parser = argparse.ArgumentParser(description="Generate a graph of time " +
            "to get each star per day along with the leaderboard. " +
            "You need to either pass a leaderboard id and session cookie " +
            "first time you run the code, or set these in an ini file. " +
            "The leaderboard data will be downloaded from the website to a " +
            "local file. It will only be redownloaded if this file is " +
            "older than 15 minutes. A png graph will be generated in the " +
            "current directory called (BOARD_ID)_(YEAR).png. Note: numpy " +
            "and matplotlib are needed for this code to work.")
    parser.add_argument("-l", "--local", action='store_true',
            help="Generate the plot using a local json file. This should " +
            "be named (BOARD_ID)_(YEAR).json (e.g. 11111_2018.json where " +
            "the default if you downloaded it manually would be 11111.json).")
    parser.add_argument("-y", "--year", help="The competition year to " +
            "fetch. If this isn't set, it will default to the most recent "
            "competition year.", type=int, default=None)
    parser.add_argument("-b", "--board_id", help="The leaderboard id for " +
            "the private leaderboard to generate a graph for.", default=None)
    parser.add_argument("-s", "--sessioncookie",
            help="The session cookie value. This is needed to access the " +
            "the online leaderboard. You can find it in firefox by going " +
            "to adventofcode.com and logging in, then opening the " +
            "developer tool (f12) and going to the storage tab. Open the " +
            "cookies section, and copy the 'Value' for 'session'. This " +
            "should be valid for around a month, after which you'll need " +
            "to get a new value. Alternatively, if you prefer you can " +
            "leave this blank, and manually download the json file from " +
            "the private leaderboard and use the -l option to prevent the " +
            "web request that would fail without this.", default=None)
    args = parser.parse_args()
    return args.local, args.year, args.board_id, args.sessioncookie

def check_config(local, board_id, sessioncookie, config_file=None,
        heading="adventofcode.com"):
    '''Check a config file for settings. Save argument settings there.'''

    # If config_file is unset, set it to the name of the script, suffixed with
    # .ini
    if config_file == None:
        my_name, my_ext = os.path.splitext(os.path.basename(__file__))
        config_file = my_name + ".ini"

    c_board_id, c_sessioncookie = "", ""
    config = configparser.ConfigParser()
    #if os.path.isfile(config_file):
    config.read(config_file)
    if heading in config:
        if 'board_id' in config[heading]:
            c_board_id = config[heading]['board_id']
        if 'sessioncookie' in config[heading]:
            c_sessioncookie = config[heading]['sessioncookie']
    else:
        config[heading] = {}
    # Update/add settings set by arguments.
    if board_id != None:
        c_board_id = board_id
    if sessioncookie != None:
        c_sessioncookie = sessioncookie

    # Update the config.
    config[heading]['board_id'] = c_board_id
    config[heading]['sessioncookie'] = c_sessioncookie
    # Output the current config to file. This will give a template for people
    # to fill in values if they're unsure of the format.
    with open(config_file, 'w') as f:
        config.write(f)

    # If necessary variables are unset, raise an error.
    if c_board_id == "":
        raise ValueError("The board_id variable has not been set.\n Please " +
                "supply this either by argument or in " + config_file)
    if c_sessioncookie == "" and not local:
        raise ValueError("The sessioncookie variable has not been set.\n" +
                " Please supply this either by argument or in " + config_file)

    return c_board_id, c_sessioncookie

def main():
    # Get any arguments.
    local, year, board_id, sessioncookie = parse_input_args()

    # Use the most recent competition year if not specified as an argument.
    if year is None:
        year = guess_year()

    # Update/set/retrieve config values from the ini file.
    board_id, sessioncookie = check_config(local, board_id, sessioncookie)

    # Fetch the leaderboard, either from local file or the web.
    board = get_leaderboard(local, year, board_id, sessioncookie)

    # Parse out a sorted table of scores and star attainment times.
    table = parse_member_details(board["members"], year)

    gen_dscore(table)

    # Generate and save a plot. The scoreboard is reproduced in the legend.
    gen_plot(table, board["update_time"],
            outputfile="%s_%d.png" % (board_id, year))

if __name__ == '__main__':
    main()
